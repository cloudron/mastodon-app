#!/bin/bash

set -eu

mkdir -p /tmp/mastodon /app/data/system /run/mastodon

if [[ ! -f /app/data/env.production ]]; then
    echo "==> Copying env template on first run"
    cp /app/pkg/env.template /app/data/env.production

    # LOCAL_DOMAIN is the federation domain. We only set this once on a fresh install
    # changing this will break federation
    sed -e "s/LOCAL_DOMAIN=.*/LOCAL_DOMAIN=${CLOUDRON_APP_DOMAIN}/g" -i /app/data/env.production
fi

echo "==> Configuring mastodon"
sed -e "s/DB_HOST=.*/DB_HOST=${CLOUDRON_POSTGRESQL_HOST}/g" \
    -e "s/DB_PORT=.*/DB_PORT=${CLOUDRON_POSTGRESQL_PORT}/g" \
    -e "s/DB_NAME=.*/DB_NAME=${CLOUDRON_POSTGRESQL_DATABASE}/g" \
    -e "s/DB_USER=.*/DB_USER=${CLOUDRON_POSTGRESQL_USERNAME}/g" \
    -e "s/DB_PASS=.*/DB_PASS=${CLOUDRON_POSTGRESQL_PASSWORD}/g" \
    -e "s/REDIS_HOST=.*/REDIS_HOST=${CLOUDRON_REDIS_HOST}/g" \
    -e "s/REDIS_PORT=.*/REDIS_PORT=${CLOUDRON_REDIS_PORT}/g" \
    -e "s/REDIS_PASSWORD=.*/REDIS_PASSWORD=${CLOUDRON_REDIS_PASSWORD}/g" \
    -e "s/SMTP_SERVER=.*/SMTP_SERVER=${CLOUDRON_MAIL_SMTP_SERVER}/g" \
    -e "s/SMTP_PORT=.*/SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}/g" \
    -e "s/SMTP_FROM_ADDRESS=.*/SMTP_FROM_ADDRESS=${CLOUDRON_MAIL_FROM}/g" \
    -e "s/SMTP_LOGIN=.*/SMTP_LOGIN=${CLOUDRON_MAIL_SMTP_USERNAME}/g" \
    -e "s/SMTP_PASSWORD=.*/SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}/g" \
    -e "s/WEB_DOMAIN=.*/WEB_DOMAIN=${CLOUDRON_APP_DOMAIN}/g" \
    -i /app/data/env.production

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Setting up OIDC"
    sed -e "s/OIDC_ENABLED=.*/OIDC_ENABLED=true/g" \
        -e "s/OIDC_DISPLAY_NAME=.*/OIDC_DISPLAY_NAME=Cloudron/g" \
        -e "s/OIDC_ISSUER=.*/OIDC_ISSUER=${CLOUDRON_OIDC_ISSUER//\//\\\/}/g" \
        -e "s/OIDC_CLIENT_ID=.*/OIDC_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}/g" \
        -e "s/OIDC_CLIENT_SECRET=.*/OIDC_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}/g" \
        -e "s/OIDC_REDIRECT_URI=.*/OIDC_REDIRECT_URI=${CLOUDRON_APP_ORIGIN//\//\\\/}\/auth\/auth\/openid_connect\/callback/g" \
        -e "s/OIDC_DISCOVERY=.*/OIDC_DISCOVERY=true/g" \
        -e "s/OIDC_SCOPE=.*/OIDC_SCOPE=openid,profile,email/g" \
        -e "s/OIDC_UID_FIELD=.*/OIDC_UID_FIELD=preferred_username/g" \
        -e "s/OIDC_SECURITY_ASSUME_EMAIL_IS_VERIFIED=.*/OIDC_SECURITY_ASSUME_EMAIL_IS_VERIFIED=true/g" \
        -i /app/data/env.production
fi

rm -f /run/mastodon/Gemfile.lock && cp /app/code/Gemfile.lock.original /run/mastodon/Gemfile.lock

# generate new secrets
if ! grep "ACTIVE_RECORD_ENCRYPTION_DETERMINISTIC_KEY" /app/data/env.production; then
    echo "ACTIVE_RECORD_ENCRYPTION_DETERMINISTIC_KEY=$(openssl rand -hex 16)" >> /app/data/env.production
fi
if ! grep "ACTIVE_RECORD_ENCRYPTION_KEY_DERIVATION_SALT" /app/data/env.production; then
    echo "ACTIVE_RECORD_ENCRYPTION_KEY_DERIVATION_SALT=$(openssl rand -hex 16)" >> /app/data/env.production
fi
if ! grep "ACTIVE_RECORD_ENCRYPTION_PRIMARY_KEY" /app/data/env.production; then
    echo "ACTIVE_RECORD_ENCRYPTION_PRIMARY_KEY=$(openssl rand -hex 16)" >> /app/data/env.production
fi

if grep -q "^SECRET_KEY_BASE=$" /app/data/env.production; then
    echo "==> Generating secrets"
    export RANDFILE=/tmp/.rnd
    sed -i -e "s/SECRET_KEY_BASE=.*/SECRET_KEY_BASE=$(openssl rand -hex 64)/" \
        -e "s/OTP_SECRET=.*/OTP_SECRET=$(openssl rand -hex 64)/" \
        /app/data/env.production

    echo "==> Generating vapid keys"
    HOME=/app/data bundle exec rake mastodon:webpush:generate_vapid_key >> /app/data/env.production

    echo "==> Init database"
    HOME=/app/data SAFETY_ASSURED=1 bundle exec rails db:schema:load db:seed

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "Disabling registration by default"
        PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} \
            -c "INSERT INTO settings (var, value) VALUES ('registrations_mode', 'none')"
    fi
else
    echo "==> Migrating database"
    HOME=/app/data SAFETY_ASSURED=1 bundle exec rails db:migrate
fi

if ! grep -q UPDATE_CHECK_URL /app/data/env.production; then
    echo -e "\nUPDATE_CHECK_URL=" >> /app/data/env.production
fi

chown -R cloudron:cloudron /tmp/mastodon /run/mastodon

if [[ "$(stat -c '%U' /app/data)" != "cloudron" ]]; then
    chown -R cloudron:cloudron /app/data/* || true # if there are no files
    chown cloudron:cloudron /app/data
fi

[[ ! -f /app/data/config.sh ]] && cp /app/pkg/config.sh /app/data/config.sh
source /app/data/config.sh

echo "==> Starting mastodon"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Mastodon

