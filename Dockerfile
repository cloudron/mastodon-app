FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 as base

FROM base AS build

# common deps to build libvips and ffmpeg
RUN apt update && \
    apt-get install -y --no-install-recommends autoconf automake libgdbm-dev libglib2.0-dev libgmp-dev libicu-dev libidn-dev libpq-dev libssl-dev libtool \
        meson nasm pkg-config shared-mime-info xz-utils libcgif-dev libexif-dev libexpat1-dev libgirepository1.0-dev libheif-dev libimagequant-dev libjpeg-turbo8-dev \
        liblcms2-dev liborc-dev libpng-dev libtiff-dev libwebp-dev libdav1d-dev liblzma-dev libmp3lame-dev libopus-dev libsnappy-dev libvorbis-dev libvpx-dev libx264-dev libx265-dev

###### libvips compilation
FROM build AS libvips

ARG VIPS_VERSION=8.15.3
ARG VIPS_URL=https://github.com/libvips/libvips/releases/download

RUN mkdir -p /usr/local/libvips/src
RUN curl -L ${VIPS_URL}/v${VIPS_VERSION}/vips-${VIPS_VERSION}.tar.xz | tar xJ --strip-components 1 -C /usr/local/libvips/src/

WORKDIR /usr/local/libvips/src

RUN meson setup build --prefix /usr/local/libvips --libdir=lib -Ddeprecated=false -Dintrospection=disabled -Dmodules=disabled -Dexamples=false
RUN cd build && ninja && ninja install

###### ffmpeg compilation
FROM build AS ffmpeg

ARG FFMPEG_VERSION=7.1
ARG FFMPEG_URL=https://ffmpeg.org/releases

RUN mkdir -p /usr/local/ffmpeg/src
RUN curl -L ${FFMPEG_URL}/ffmpeg-${FFMPEG_VERSION}.tar.xz | tar xJ --strip-components 1 -C /usr/local/ffmpeg/src/

WORKDIR /usr/local/ffmpeg/src

RUN ./configure --prefix=/usr/local/ffmpeg --toolchain=hardened --disable-debug \
        --disable-devices --disable-doc --disable-ffplay --disable-network --disable-static \
        --enable-ffmpeg --enable-ffprobe --enable-gpl --enable-libdav1d --enable-libmp3lame \
        --enable-libopus --enable-libsnappy --enable-libvorbis --enable-libvpx \
        --enable-libwebp --enable-libx264 --enable-libx265 --enable-shared --enable-version3 && \
    make -j$(nproc) && \
    make install

##### Mastodon
FROM base AS mastodon

RUN apt update && \
    apt-get install -y --no-install-recommends libexpat1 libglib2.0-0 libicu70 libidn12 libidn-dev libpq5 libreadline8 libssl3 libyaml-0-2 libcgif0 libexif12 libheif1 libimagequant0 libjpeg62 \
        libjpeg-turbo8 liblcms2-2 liborc-0.4-0 libpng16-16 libtiff5 libwebp7 libwebpdemux2 libwebpmux3 libdav1d5 libmp3lame0 libopencore-amrnb0 libopencore-amrwb0 libopus0 libsnappy1v5 libtheora0 \
        libvorbis0a libvorbisenc2 libvorbisfile3 libvpx7 libx264-163 libx265-199 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# rbenv since we need specific ruby
RUN mkdir -p /usr/local/rbenv && curl -LSs "https://github.com/rbenv/rbenv/archive/refs/tags/v1.3.0.tar.gz" | tar -xz -C /usr/local/rbenv --strip-components 1 -f -
ENV PATH /usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT /home/cloudron/rbenv
RUN mkdir -p "$(rbenv root)"/plugins/ruby-build && curl -LSs "https://github.com/rbenv/ruby-build/archive/refs/tags/v20241007.tar.gz" | tar -xz -C "$(rbenv root)"/plugins/ruby-build --strip-components 1 -f -

# install specific ruby version (https://github.com/mastodon/mastodon/blob/main/Dockerfile)
ARG RUBY_VERSION=3.3.5
RUN rbenv install ${RUBY_VERSION}
ENV PATH ${RBENV_ROOT}/versions/${RUBY_VERSION}/bin:$PATH
RUN gem install --no-document bundler

ARG NODE_VERSION=20.18.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=tootsuite/mastodon versioning=semver extractVersion=^v(?<version>.+)$
ARG MASTODON_VERSION=4.3.4

ENV RAILS_ENV production
ENV NODE_ENV production
ARG RAILS_SERVE_STATIC_FILES="true"
ARG RUBY_YJIT_ENABLE="1"

ENV MALLOC_CONF "narenas:2,background_thread:true,thp:never,dirty_decay_ms:1000,muzzy_decay_ms:0"
ENV MASTODON_USE_LIBVIPS true

COPY --from=libvips /usr/local/libvips/bin /usr/local/bin
COPY --from=libvips /usr/local/libvips/lib /usr/local/lib

COPY --from=ffmpeg /usr/local/ffmpeg/bin /usr/local/bin
COPY --from=ffmpeg /usr/local/ffmpeg/lib /usr/local/lib

# Smoketest media processors
RUN ldconfig && \
    vips -v && \
    ffmpeg -version && \
    ffprobe -version

RUN curl -L https://github.com/tootsuite/mastodon/archive/v${MASTODON_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    bundle config --local set deployment 'true' && \
    bundle config --local set without 'development test' && \
    bundle config --local set silence_root_warning true && \
    bundle install && \
    bundle clean --force && \
    rm -rf ~/.bundle /usr/local/bundle/cache

RUN corepack enable && \
    corepack prepare --activate

RUN yarn workspaces focus --production @mastodon/mastodon
RUN yarn install

RUN ldconfig && \
    SECRET_KEY_BASE_DUMMY=1 bundle exec rails assets:precompile && \
    rm -rf /app/code/tmp

# Precompile bootsnap code for faster Rails startup
RUN bundle exec bootsnap precompile --gemfile app/ lib/

# https://github.com/rubygems/bundler/issues/5245 means that bundle exec writes to Gemfile.lock
RUN ln -fs /run/mastodon/bullet.log /app/code/log/bullet.log && \
    rm -rf /app/code/tmp && ln -fs /tmp/mastodon /app/code/tmp && \
    mv /app/code/Gemfile.lock /app/code/Gemfile.lock.original && ln -s /run/mastodon/Gemfile.lock /app/code/Gemfile.lock

# add nginx config
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/mastodon.conf /etc/nginx/sites-available/mastodon
RUN ln -s /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon

# add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/mastodon/supervisord.log /var/log/supervisor/supervisord.log

RUN ln -fs /app/data/env.production /app/code/.env.production
RUN ln -fs /app/data/system /app/code/public/system

COPY start.sh cleanup.sh config.sh env.template cache-env.sh.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
