#!/bin/bash

set -eu

echo "=> Cleanup"
cd /app/code

if [[ ! -f /app/data/cache-env.sh ]]; then
    echo "==> Creating initial cache-env.sh"
    cp /app/pkg/cache-env.sh.template /app/data/cache-env.sh
fi

source /app/data/cache-env.sh

echo "=> Retention days set to ${CACHE_RETENTION_DAYS}"

echo "==> media cache ..."
./bin/tootctl media remove --days=${CACHE_RETENTION_DAYS}

echo "==> orphaned media ..."
./bin/tootctl media remove-orphans

echo "==> preview cards ..."
./bin/tootctl preview-cards remove --days=${CACHE_RETENTION_DAYS}

echo "==> prune profiles ..."
./bin/tootctl media remove --prune-profiles --days=${CACHE_RETENTION_DAYS}

echo "==> remove headers ..."
./bin/tootctl media remove --remove-headers --days=${CACHE_RETENTION_DAYS}

echo "==> clear cache ..."
/app/code/bin/tootctl cache clear

echo "==> remove unreferenced statuses ..."
/app/code/bin/tootctl statuses remove --days=${CACHE_RETENTION_DAYS}

echo "==> prune accounts ..."
/app/code/bin/tootctl accounts prune
